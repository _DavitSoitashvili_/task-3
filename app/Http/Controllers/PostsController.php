<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    function showPosts()
    {
        $posts = Post::all();
        return view("show_posts", compact("posts"));
    }

    function createPost()
    {
        $tags = Tag::all();
        return \view("create_post", compact("tags"));
    }

    function savePost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $post = Post::create([
            'title' => $request->get("title"),
            'post_body' => $request->get("post_body"),
            'author' => $request->get("author"),
        ]);
        $tags = $request->get("tags");
        $post->tags()->sync($tags);
        return redirect()->action([PostsController::class, 'showPosts']);
    }

    function editPost($id)
    {
        $post = Post::find($id);
        $tags = Tag::all();

        return view("edit_post", compact("post", "tags"));
    }

    function saveEditedPost(Request $request)
    {
        $post = Post::find($request->get("id"));
        $post->title = $request->get("title");
        $post->post_body = $request->get("post_body");
        $post->author = $request->get("author");
        $post->save();

        $selectedTags = $request->get("tags");
        $post->tags()->sync($selectedTags);

        return redirect()->action([PostsController::class, 'showPosts']);

    }

    function deletePost($id): \Illuminate\Http\RedirectResponse
    {
        $article = Post::findOrFail($id);
        $article->delete();
        return redirect()->action([PostsController::class, 'showPosts']);
    }
}
