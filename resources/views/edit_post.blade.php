@extends("layout")
@section("Title")
    Edit Post
@endsection
@section("Content")
    <form style="margin-left: 20px" method="post" action="{{route("save.edited.post")}}">
        @csrf
        @method("PUT")
        <input type="hidden" name="id" value="{{$post->id}}">
        <div>
            <label for="title">Post Title</label>
            <br>
            <input required="required" id="title" type="text" name="title" value="{{$post->title}}">
        </div>
        <div>
            <label for="post_body">Post Description</label>
            <br>
            <input required="required" id="post_body" type="text" name="post_body" value="{{$post->post_body}}">
        </div>
        <div>
            <label for="author">Post Author</label>
            <br>
            <input required="required" id="author" type="text" name="author" value="{{$post->author}}">
        </div>

        <div class="form-group">
            <label for="tags">tags</label>
            <br>
            <select required="required" name="tags[]" id="tags" multiple>
                @foreach($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->tag_name}}</option>
                @endforeach
            </select>
            <hr>
            <label for="selected_tags">Selected Tags</label>
            <div id="selected_tags">
                <ul>
                    @foreach($post->tags as $selectedTag)
                        <li>{{$selectedTag->tag_name}}</li>
                    @endforeach
                </ul>
            </div>
        </div>

        <button class="btn btn-primary" type="submit">Edit Post</button>
    </form>
@endsection
