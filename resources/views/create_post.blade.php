@extends("layout")
@section("Title")
    Create Post
@endsection
@section("Content")
    <form method="post" action="{{route("save.post")}}">
        @csrf
        <div>
            <label for="title">Post Title</label>
            <br>
            <input required="required" id="title" type="text" name="title">
        </div>
        <div>
            <label for="post_body">Post Description</label>
            <br>
            <input required="required" id="post_body" type="text" name="post_body">
        </div>
        <div>
            <label for="author">Post Author</label>
            <br>
            <input required="required" id="author" type="text" name="author">
        </div>

        <div class="form-group">
            <label for="tags">tags</label>
            <br>
            <select required="required" name="tags[]" id="tags" multiple>
                @foreach($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->tag_name}}</option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-primary" type="submit">Create Post</button>
    </form>
@endsection
