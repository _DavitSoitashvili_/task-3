@extends("layout")
@section("Title")
    Posts
@endsection
@section("Content")
    <a style="margin-left: 20px; margin-top: 50px" href={{route('create.post')}}>Create Post</a>
    <div id="posts">
        @foreach($posts as $post)
            <div class="post" style="border: 1px solid black; border-radius: 20px; margin:20px; padding:20px">
                <h4>Post Title : {{$post->title}}</h4>
                <h5>Post Description : {{$post->post_body}}</h5>
                <h5>Post Author : {{$post->author}}</h5>
                <ul style="font-size: 20px; color: red">
                    @foreach($post->tags as $tag)
                        <li>{{$tag->tag_name}}</li>
                    @endforeach
                </ul>
                <a href={{route('edit.post', $post->id, false)}}>Edit</a>
                <a href={{route('delete.post', $post->id, false)}}>Delete</a>
            </div>
        @endforeach
    </div>
@endsection
