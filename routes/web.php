<?php

use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [PostsController::class, "showPosts"])->name("show.posts");
Route::get("/create_post", [PostsController::class, "createPost"])->name("create.post");
Route::post("/save_post/", [PostsController::class, "savePost"])->name("save.post");
Route::get("/edit_post/{id}",   [PostsController::class, "editPost"])->name("edit.post");
Route::put("/save_edited_post/", [PostsController::class, "saveEditedPost"])->name("save.edited.post");
Route::get("/delete_post/{id}", [PostsController::class, "deletePost"])->name("delete.post");
